#!/bin/bash
echo "Shell script auto create remote.zip file"
#redirecto folder remote"
cd /home/ldson/webmely/gridbill_remote
#zip files and folder --> remote.zip"
zip -r remote.zip package.json ./routes
version=""
#read pakage.json
while read line; do
  if [ `echo $line | grep -c "version"` -gt -0 ]
  then
	   description=`echo $line | sed -e 's/^.*"version"[ ]*:[ ]*"//' -e 's/".*//'`
	   version=$description
	   #mkdir /home/ldson/Desktop/"$v"
  fi
done < package.json
#check folder exits and moving remote.zip
if [ -d "/home/ldson/Desktop/remote_version" ]; then
  echo "Folder remote_version is exits and moving"
  mkdir /home/ldson/Desktop/remote_version/"$version"
  mv remote.zip /home/ldson/Desktop/remote_version/"$version"
else
  echo "Folder remote_version is not exits"
  echo "Create folder remote_version"
  mkdir /home/ldson/Desktop/remote_version/
  mkdir /home/ldson/Desktop/remote_version/"$version"
  echo "Moving remote.zip to folder remote_version"
  mv remote.zip /home/ldson/Desktop/remote_version/"$version"
fi
